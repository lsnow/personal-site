# logansnow.com

A resume showcase website for Logan Snow (live at [logansnow.com](https://logansnow.com))

  - Recent projects
  - Interesting and important links
  - Looks cool

# New Features!

  - Dynamic loading of projects
  - Responsiveness


### Tech

logansnow.com uses a number of open source projects to work properly:

* [Vue.js] - Web framework
* [VS Code] - Text editor
* [Bootstrap-Vue] - Responsive web design for Vue.js
* [Apache] - Webserver

And of course logansnow.com itself is open source with a [public repository][dill]
 on GitLab.

### Installation

logansnow.com requires [Node.js](https://nodejs.org/) v4+ to run (NVM suggested).

Clone the repo:

```sh
$ git clone git@gitlab.com:lsnow/personal-site.git
```

Install dependencies:

```sh
$ cd personal-site
$ npm install
```

For developer environments...

```sh
$ npm run serve
```

For production environments...

```sh
$ npm run build
```

For deployment on piserver...

```sh
$ cd src/
$ sudo chmod +x build.sh
$ sudo ./build.sh
```

MIT License

Copyright (c) 2018 Logan Snow


   [Vue.js]: <https://vuejs.org>
   [dill]: <https://gitlab.com/lsnow/personal-site>
   [VS Code]: <https://code.visualstudio.com>
   [Bootstrap-Vue]: <https://bootstrap-vue.js.org/>
   [Apache]: <https://httpd.apache.org>
