import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.config.productionTip = false

Vue.use(BootstrapVue)

new Vue({
  router,
  head: {
    title: function () {
      return "Hi, I'm Logan"
    }
  },
  render: h => h(App)
}).$mount('#app')
