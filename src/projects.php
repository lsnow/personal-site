<?php

$dir = 'projects';
$files = scandir($dir, 1);
$arr = array();

for($counter = 1; $counter < sizeof($files) - 2; $counter = $counter + 2) {
        $title = '';
        $desc = '';
        $img = 'projects/' . $files[$counter];
        $file = fopen("projects/" . $files[$counter - 1],"r");
        $title = fgets($file);
        while(! feof($file))
        {
                $desc = $desc . fgets($file);
        }
        array_push($arr, array('project_img' => $img, 'project_desc' => $desc, 'project_title' => $title));
}

echo(json_encode($arr));

?>
