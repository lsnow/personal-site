#!/bin/bash
echo "Running vue build"
npm run build
echo "Removing old dist folder"
rm -r /var/www/html/dist
echo "Linking dist folder"
cp -R ../dist /var/www/html/
echo "Linking projects folder"
ln -s /serverdata/data/logan/files/Website/projects /var/www/html/dist/
echo "Linking Resume"
ln -s /serverdata/data/logan/files/Documents/Logan\'s\ Resume.pdf /var/www/html/dist/
echo "Copying php server files"
cp *.php /var/www/html/dist/
echo "Done"
